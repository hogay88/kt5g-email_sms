package kr.wisenut.kt5g.config;

import org.aeonbits.owner.Config;

@Config.Sources({"file:${user.dir}/config/api.properties",
        "file:${user.dir}/..//config/api.properties"})
public interface ApiConfig extends Config {

    //username
    @Key("api.username")
    @DefaultValue("hogay88")
    String userName();

    //key
    @Key("api.key")
    @DefaultValue("bE8jWn5oRz4E4TB")
    String key();

    // url
    @Key("api.email.url")
    @DefaultValue("https://directsend.co.kr/index.php/api_v2/mil_change_word")
    String emailUrl();

    //email 제목
    @Key("api.email.subject")
    @DefaultValue("제목")
    String subject();

    //email 내용
    @Key("api.email.body")
    @DefaultValue("내용")
    String body();

    //sender
    @Key("api.email.sender")
    @DefaultValue("default@gmail.com")
    String emailSender();

    //sender_name
    @Key("api.email.sender.name")
    @DefaultValue("보낸이")
    String emailSenderName();

    // url
    @Key("api.sms.url")
    @DefaultValue("https://directsend.co.kr/index.php/api_v2/sms_change_word")
    String smsUrl();

    //message 제목
    @Key("api.sms.title")
    @DefaultValue("제목")
    String title();

    //message 내용
    @Key("api.sms.message")
    @DefaultValue("내용")
    String message();

    //sender(번호)
    @Key("api.sms.sender")
    @DefaultValue("0101234567")
    String smsSender();

}

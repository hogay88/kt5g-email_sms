package kr.wisenut.kt5g.vo.data;


public class AbstractVo {
    protected Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}

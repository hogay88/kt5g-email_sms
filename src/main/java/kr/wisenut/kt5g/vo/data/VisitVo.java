package kr.wisenut.kt5g.vo.data;

public class VisitVo extends AbstractVo{
    private String visitorName;
    private String visitorId;
    private Long locationId;
    public String locationName;
    private String regDt;
    private String visitStartDt;
    private String visitEndDt;
    private String notificationDt;
    private String comment;
    private String hostId;
    private String hostName;
    private int acceptedStatus;
    private String acceptedId;
    private String acceptedDt;
    private int interviewComplete;
    private String interviewLink;

    public String getVisitorName() {
        return visitorName;
    }

    public void setVisitorName(String visitorName) {
        this.visitorName = visitorName;
    }

    public String getVisitorId() {
        return visitorId;
    }

    public void setVisitorId(String visitorId) {
        this.visitorId = visitorId;
    }

    public Long getLocationId() {
        return locationId;
    }

    public void setLocationId(Long locationId) {
        this.locationId = locationId;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    public String getRegDt() {
        return regDt;
    }

    public void setRegDt(String regDt) {
        this.regDt = regDt;
    }

    public String getVisitStartDt() {
        return visitStartDt;
    }

    public void setVisitStartDt(String visitStartDt) {
        this.visitStartDt = visitStartDt;
    }

    public String getVisitEndDt() {
        return visitEndDt;
    }

    public void setVisitEndDt(String visitEndDt) {
        this.visitEndDt = visitEndDt;
    }

    public String getNotificationDt() {
        return notificationDt;
    }

    public void setNotificationDt(String notificationDt) {
        this.notificationDt = notificationDt;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getHostId() {
        return hostId;
    }

    public void setHostId(String hostId) {
        this.hostId = hostId;
    }

    public String getHostName() {
        return hostName;
    }

    public void setHostName(String hostName) {
        this.hostName = hostName;
    }

    public int getAcceptedStatus() {
        return acceptedStatus;
    }

    public void setAcceptedStatus(int acceptedStatus) {
        this.acceptedStatus = acceptedStatus;
    }

    public String getAcceptedId() {
        return acceptedId;
    }

    public void setAcceptedId(String acceptedId) {
        this.acceptedId = acceptedId;
    }

    public String getAcceptedDt() {
        return acceptedDt;
    }

    public void setAcceptedDt(String acceptedDt) {
        this.acceptedDt = acceptedDt;
    }

    public int getInterviewComplete() {
        return interviewComplete;
    }

    public void setInterviewComplete(int interviewComplete) {
        this.interviewComplete = interviewComplete;
    }

    public String getInterviewLink() {
        return interviewLink;
    }

    public void setInterviewLink(String interviewLink) {
        this.interviewLink = interviewLink;
    }

    @Override
    public String toString() {
        return "VisitVo{" +
                "visitorName='" + visitorName + '\'' +
                ", visitorId='" + visitorId + '\'' +
                ", locationId=" + locationId +
                ", locationName='" + locationName + '\'' +
                ", regDt='" + regDt + '\'' +
                ", visitStartDt='" + visitStartDt + '\'' +
                ", visitEndDt='" + visitEndDt + '\'' +
                ", notificationDt='" + notificationDt + '\'' +
                ", comment='" + comment + '\'' +
                ", hostId='" + hostId + '\'' +
                ", hostName='" + hostName + '\'' +
                ", acceptedStatus=" + acceptedStatus +
                ", acceptedId='" + acceptedId + '\'' +
                ", acceptedDt='" + acceptedDt + '\'' +
                ", interviewComplete=" + interviewComplete +
                ", interviewLink='" + interviewLink + '\'' +
                '}';
    }
}

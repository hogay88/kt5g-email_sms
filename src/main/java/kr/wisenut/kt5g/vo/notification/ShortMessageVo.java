package kr.wisenut.kt5g.vo.notification;

import java.util.Objects;

public class ShortMessageVo {
    String title;
    String message;
    // 전화번호
    String sender;


    public ShortMessageVo() {
    }

    public ShortMessageVo(String title, String message, String sender) {
        this.title = title;
        this.message = message;
        this.sender = sender;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ShortMessageVo that = (ShortMessageVo) o;
        return Objects.equals(title, that.title) &&
                Objects.equals(message, that.message) &&
                Objects.equals(sender, that.sender);
    }

    @Override
    public int hashCode() {

        return Objects.hash(title, message, sender);
    }

    @Override
    public String toString() {
        return "ShortMessageVo{" +
                "title='" + title + '\'' +
                ", message='" + message + '\'' +
                ", sender='" + sender + '\'' +
                '}';
    }

}

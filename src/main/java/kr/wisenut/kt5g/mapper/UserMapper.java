package kr.wisenut.kt5g.mapper;

import kr.wisenut.kt5g.vo.data.UserVo;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * User 정보를 가지고 있는 Mapper
 */
public class UserMapper {

    private Map<Long, UserVo> dataMap;

    public UserMapper() {
        dataMap = new HashMap<>();

        /**
         * Dummy Data로 User 정보 초기화
         */
        UserVo userVo = new UserVo();
        userVo.setId(1L);
        userVo.setUserId("123");
        userVo.setEmail("origin103242@gmail.com");
        userVo.setPhoneNumber("010-7249-5842");
        userVo.setNotificationTypes(Arrays.asList("E-MAIL", "SMS"));
        dataMap.put(1L, userVo);
    }

    public UserVo getByUserId(String userId) {
        for (UserVo user : dataMap.values()) {
            if (user.getUserId().equals(userId)) {
                return user;
            }
        }
        return null;
    }

}

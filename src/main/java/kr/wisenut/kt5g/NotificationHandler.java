package kr.wisenut.kt5g;

import kr.wisenut.kt5g.notifier.EmailNotifier;
import kr.wisenut.kt5g.notifier.SmsNotifier;
import kr.wisenut.kt5g.vo.data.UserVo;
import kr.wisenut.kt5g.vo.data.VisitVo;
import kr.wisenut.kt5g.mapper.UserMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.List;

public class NotificationHandler {
    private static final Logger logger = LoggerFactory.getLogger(NotificationHandler.class);
    //User 정보를 가지고 있는 Mapper
    private final UserMapper userMapper;

    public NotificationHandler(UserMapper userMapper) {
        this.userMapper = userMapper;
    }

    public void notify(final VisitVo visitVo) throws IOException {
        //방문 정보의 방문자 ID로 User 정보를 조회
        UserVo userVo = userMapper.getByUserId(visitVo.getVisitorId());
        List<String> notificationTypes = userVo.getNotificationTypes();
        if (notificationTypes.size() == 0) {
            return;
        }
        for (String type : notificationTypes) {
            switch (type) {
                case "E-MAIL":
                    new EmailNotifier().send(userVo, visitVo);
                    break;
                case "SMS":
                    new SmsNotifier().send(userVo, visitVo);
                    break;
                default:
                    logger.warn("This type is an undefined notification type.");
                    break;
            }
        }
    }

    public static void main(String[] args) throws IOException {
        UserMapper userMapper = new UserMapper();
        NotificationHandler notificationHandler = new NotificationHandler(userMapper);

        /**
         *  방문자 정보 Dummy 초기화
         */

        VisitVo visitVo = new VisitVo();
        visitVo.setVisitorId("123");
        visitVo.setVisitStartDt("2020-11-13T04:23:02.465Z");
        visitVo.setLocationName("와이즈넛 6층 회의실");
        visitVo.setInterviewLink("http://211.39.140.86:18009/interview/user/123");

        notificationHandler.notify(visitVo);
    }
}

package kr.wisenut.kt5g.notifier;

import com.google.gson.JsonObject;
import kr.wisenut.kt5g.vo.data.UserVo;
import kr.wisenut.kt5g.vo.data.VisitVo;
import kr.wisenut.kt5g.vo.notification.ShortMessageVo;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URL;
import java.util.Locale;

public class SmsNotifier extends Notifier {

    private static final Logger logger = LoggerFactory.getLogger(SmsNotifier.class);
    private final ShortMessageVo messageVo;

    public SmsNotifier() throws IOException {
        obj = new URL(config.smsUrl());
        setConnection();
        messageVo = new ShortMessageVo(config.title(), config.message(), config.smsSender());
    }

    @Override
    protected String generateMessage(VisitVo visitVo) {
        // parse date
        DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        DateTime dateTime = formatter.parseDateTime(visitVo.getVisitStartDt()).plusHours(9);
        String strDate = dateTime.toString("yyyy.MM.dd (E), a hh:mm:ss", Locale.KOREA);

        return visitVo.getVisitorId() + " 님의 금일 방문에 대해 알려드립니다." +
                "\\r\\n" + "방문 시간 : " + strDate +
                "\\r\\n" + "방문 장소 : " + visitVo.getLocationName() +
                "\\r\\n" + "문진표 작성 : " + visitVo.getInterviewLink();
    }

    @Override
    public void send(final UserVo userVo, final VisitVo visitVo) throws IOException {
        String title = messageVo.getTitle();
//        String message = messageVo.getMessage();
        String message = generateMessage(visitVo);
        String sender = messageVo.getSender();

//        JSONObject jsonObj = new JSONObject();
//        jsonObj.put("mobile", userVo.getPhoneNumber());
        JsonObject jsonObj = new JsonObject();
        jsonObj.addProperty("mobile", userVo.getPhoneNumber())  ;
        String receiver = jsonObj.toString();
        receiver = "[" + receiver + "]";

        String urlParameters = "\"title\":\"" + title + "\" "
                + ", \"message\":\"" + message + "\" "
                + ", \"sender\":\"" + sender + "\" "
                + ", \"username\":\"" + config.userName() + "\" "
                + ", \"receiver\":" + receiver
                + ", \"key\":\"" + config.key() + "\" "
                + ", \"type\":\"" + "java" + "\" ";

        urlParameters = "{" + urlParameters + "}";

        logger.debug(urlParameters);

        this.requestSendApi(urlParameters);
    }

}

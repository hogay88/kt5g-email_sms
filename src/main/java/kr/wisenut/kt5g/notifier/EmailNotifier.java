package kr.wisenut.kt5g.notifier;

import com.google.gson.JsonObject;
import kr.wisenut.kt5g.vo.data.UserVo;
import kr.wisenut.kt5g.vo.data.VisitVo;
import kr.wisenut.kt5g.vo.notification.EmailVo;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Locale;

public class EmailNotifier extends Notifier {

    private static final Logger logger = LoggerFactory.getLogger(EmailNotifier.class);
    private final EmailVo emailVo;

    public EmailNotifier() throws IOException {
        obj = new java.net.URL(config.emailUrl());
        setConnection();
        emailVo = new EmailVo(config.subject(), config.body(), config.emailSender(), config.emailSenderName());
    }

    @Override
    protected String generateMessage(VisitVo visitVo) {
        // parse date
        DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        DateTime dateTime = formatter.parseDateTime(visitVo.getVisitStartDt()).plusHours(9);
        String strDate = dateTime.toString("yyyy.MM.dd (E), a hh:mm:ss", Locale.KOREA);

        return visitVo.getVisitorId() + " 님의 금일 방문에 대해 알려드립니다." +
                "<br>" + "방문 시간 : " + strDate +
                "<br>" + "방문 장소 : " + visitVo.getLocationName() +
                "<br>" + "<a href="+visitVo.getInterviewLink()+">문진표 작성</a>";
    }

    @Override
    public void send(final UserVo userVo, final VisitVo visitVo) throws IOException {
        String subject = emailVo.getSubject();
        //String body = emailVo.getBody();
        String body = generateMessage(visitVo);
        String sender = emailVo.getSender();
        String sender_name = emailVo.getSender_name();

        JsonObject jsonObj = new JsonObject();
        jsonObj.addProperty("email", userVo.getEmail());
        String receiver = jsonObj.toString();
        receiver = "[" + receiver + "]";

        String urlParameters = "\"subject\":\"" + subject + "\" "
                + ", \"body\":\"" + body + "\" "
                + ", \"sender\":\"" + sender + "\" "
                + ", \"sender_name\":\"" + sender_name + "\" "
                + ", \"username\":\"" + config.userName() + "\" "
                + ", \"receiver\":" + receiver
                + ", \"key\":\"" + config.key() + "\" ";
        urlParameters = "{" + urlParameters + "}";        //JSON 데이터

        logger.debug(urlParameters);

        requestSendApi(urlParameters);
    }

}

package kr.wisenut.kt5g.notifier;

import com.google.gson.Gson;
import kr.wisenut.kt5g.NotificationHandler;
import kr.wisenut.kt5g.config.ApiConfig;
import kr.wisenut.kt5g.vo.data.UserVo;
import kr.wisenut.kt5g.vo.data.VisitVo;
import kr.wisenut.kt5g.vo.notification.NotificationResponseVo;
import org.aeonbits.owner.ConfigCache;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.util.Locale;

public abstract class Notifier {

    protected static final ApiConfig config = ConfigCache.getOrCreate(ApiConfig.class);
    private static final Logger logger = LoggerFactory.getLogger(NotificationHandler.class);
    protected java.net.URL obj;
    protected HttpURLConnection con;

    public static final String LINE_FEED_EMAIL = "<br>";
    public static final String LINE_FEED_SMS = "\\r\\n";

    public abstract void send(final UserVo userVo, final VisitVo visitVo) throws IOException;

    protected void setConnection() throws IOException {
        con = (HttpURLConnection) obj.openConnection();
        con.setRequestProperty("Cache-Control", "no-cache");
        con.setRequestProperty("Content-Type", "application/json;charset=utf-8");
        con.setRequestProperty("Accept", "application/json");
    }

    protected void requestSendApi(final String urlParams) throws IOException {
        System.setProperty("jsse.enableSNIExtension", "false");
        con.setDoOutput(true);
        OutputStreamWriter wr = new OutputStreamWriter(con.getOutputStream());
        wr.write(urlParams);
        wr.flush();
        wr.close();

        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuilder response = new StringBuilder();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }

        NotificationResponseVo responseVo = new Gson().fromJson(response.toString(), NotificationResponseVo.class);
        if (!responseVo.getStatus().equals("0")) {
            logger.info("Failed to send notification. {}", response.toString());
        }

        in.close();
    }

    protected String generateMessage(final VisitVo visitVo) {
        // parse date
        DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        DateTime dateTime = formatter.parseDateTime(visitVo.getVisitStartDt()).plusHours(9);
        String strDate = dateTime.toString("yyyy.MM.dd (E), a hh:mm:ss", Locale.KOREA);

        return visitVo.getVisitorId() + " 님의 금일 방문에 대해 알려드립니다." +
                "<br>" + "방문 시간 : " + strDate +
                "<br>" + "방문 장소 : " + visitVo.getLocationName();
    }
}
